from config.database import Base
from sqlalchemy import Column, Integer, String

class Compu(Base):
    __tablename__ = "computadoras"

    id=Column(Integer, primary_key = True)
    Marca = Column (String)
    Modelo = Column (String)
    Color = Column(String)
    Ram = Column (String)
    Almacenamiento = Column (String)